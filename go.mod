module bitbucket.org/cornjacket/discovery

go 1.13

require (
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/armon/circbuf v0.0.0-20190214190532-5111143e8da2 // indirect
	github.com/armon/go-metrics v0.3.4 // indirect
	github.com/aws/aws-sdk-go v1.35.20 // indirect
	github.com/elazarl/go-bindata-assetfs v1.0.1 // indirect
	github.com/fsouza/go-dockerclient v1.6.6 // indirect
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/hashicorp/consul v0.8.3
	github.com/hashicorp/go-checkpoint v0.5.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.1 // indirect
	github.com/hashicorp/go-memdb v1.3.0 // indirect
	github.com/hashicorp/go-msgpack v1.1.5 // indirect
	github.com/hashicorp/go-rootcerts v1.0.2 // indirect
	github.com/hashicorp/go-sockaddr v1.0.2 // indirect
	github.com/hashicorp/go-uuid v1.0.2 // indirect
	github.com/hashicorp/go-version v1.2.1 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/hashicorp/hil v0.0.0-20200423225030-a18a1cd20038 // indirect
	github.com/hashicorp/net-rpc-msgpackrpc v0.0.0-20151116020338-a14192a58a69 // indirect
	github.com/hashicorp/raft v1.2.0 // indirect
	github.com/hashicorp/raft-boltdb v0.0.0-20191021154308-4207f1bf0617 // indirect
	github.com/hashicorp/scada-client v0.0.0-20160601224023-6e896784f66f // indirect
	github.com/hashicorp/serf v0.9.5 // indirect
	github.com/hashicorp/yamux v0.0.0-20200609203250-aecfd211c9ce // indirect
	github.com/miekg/dns v1.1.35 // indirect
	github.com/mitchellh/cli v1.1.2 // indirect
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/ryanuber/columnize v2.1.2+incompatible // indirect
	github.com/shirou/gopsutil v3.20.10+incompatible // indirect
	google.golang.org/api v0.34.0 // indirect
)
