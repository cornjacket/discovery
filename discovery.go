package discovery

// Purpose: Service is a client that interfces with Consul Discovery and registers a service or requests info on a service

import (
	// Standard library packets

	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	// Third party packages
	"bitbucket.org/cornjacket/discovery/env"
	consulapi "github.com/hashicorp/consul/api"
)

var consulUrl = ""

func init() {
	consulUrl = env.GetVar("CONSUL_HTTP_ADDR", "", false)
}

func RegisterService(serviceName, portNum string) {
	if UrlProvided() {
		config := consulapi.DefaultConfig()
		consul, err := consulapi.NewClient(config)
		if err != nil {
			log.Println(err)
		}

		var registration = new(consulapi.AgentServiceRegistration)

		registration.ID = serviceName
		registration.Name = serviceName

		address, _ := os.Hostname()
		registration.Address = address
		port, _ := strconv.Atoi(portNum)
		registration.Port = port

		registration.Check = new(consulapi.AgentServiceCheck)
		registration.Check.HTTP = fmt.Sprintf("http://%s:%v/info", address, port)
		registration.Check.Interval = "5s"
		registration.Check.Timeout = "3s"

		//consul.Agent().ServiceRegister(registration)
		maxRetries := 10 // testing
		for i := 0; i < maxRetries; i++ {
			err = consul.Agent().ServiceRegister(registration)
			if err != nil {
				log.Println(err)
				log.Println("consul error - retrying after 3 seconds")
				time.Sleep(time.Duration(3) * time.Second)
			} else {
				log.Println("consul replied")
				break
			}
		}
		if err != nil {
			log.Fatal("Unable to register with Discovery. Quitting!!")
		}

	}
}

// Function will fatally terminate if Consul is down or Consul does not have details for serviceName
// Function will perform up to 20 retries (3 second spacing) - retries based on Consul down or Consul does
// not have details for serviceName.
// This function can be improved by extracting the services map as a package level variable
func LookupService(serviceName string, maxRetries int) (address, port string, err error) {
	config := consulapi.DefaultConfig()
	consul, err := consulapi.NewClient(config)
	if err != nil {
		fmt.Println(err)
		//log.Fatal("Consul not responding. Quitting!!")
		return "", "", err
	}

	var services map[string]*consulapi.AgentService

	for i := 0; i < maxRetries; i++ {
		services, err = consul.Agent().Services()
		if err != nil {
			log.Printf("consul error: %v", err)
		} else if service, ok := services[serviceName]; ok {
			address = service.Address
			port = strconv.Itoa(service.Port)
			log.Println("consul replied")
			break
		} else {
			log.Printf("consul error - service: %s not found.", serviceName)
		}
		if i != (maxRetries-1) {
			time.Sleep(time.Duration(3) * time.Second)
		}

	}
	if err != nil {
		fmt.Println(err)
		//log.Fatal("Consul not responding. Quitting!!")
		return "", "", err
	}
	if err == nil && address == "" {
		log.Println("Service not found in Consul")
		err = errors.New("service not found")
	}

	//url = fmt.Sprintf("http://%s:%v/info", address, port)
	return address, port, err
}

func UrlProvided() bool {
	return consulUrl != ""
}
